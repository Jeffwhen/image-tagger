'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import * as Utils from './utils.js';

function styleObjectToString(obj) {
  let str = '';
  for (var key in obj) {
    str += key + ': ' + obj[key] + ';';
  }
  return str;
}

function attrOptionForEach(options, callback) {
  if (!options) return;
  options.forEach(option => {
    callback(option);
    attrOptionForEach(option.children || [], callback);
  });
}

let _thumbWidth = 180;
let _thumbHeight = 180;
function genImageElem(data, thumbWidth, thumbHeight, params) {
  params = params || {};
  _thumbWidth = thumbWidth ? thumbWidth : _thumbWidth;
  _thumbHeight = thumbHeight ? thumbHeight : _thumbHeight;
  let crop = {
    xmin: 1,
    xmax: 0,
    ymin: 1,
    ymax: 0
  };
  let hasCrop = false;
  data.objs = data.objs || [];
  data.objs.forEach(obj => {
    obj.bndBoxes = obj.bndBoxes || [];
    obj.bndBoxes.forEach(box => {
      hasCrop = true;
      crop.xmin = box.xmin < crop.xmin ? box.xmin : crop.xmin;
      crop.xmax = box.xmax > crop.xmax ? box.xmax : crop.xmax;
      crop.ymin = box.ymin < crop.ymin ? box.ymin : crop.ymin;
      crop.ymax = box.ymax > crop.ymax ? box.ymax : crop.ymax;
    });
  });
  if (hasCrop && (crop.xmin > crop.xmax || crop.ymin > crop.ymax)) {
    console.error(data);
    throw new Error('invalid box');
    crop = {
      xmin: 1,
      xmax: 0,
      ymin: 1,
      ymax: 0
    };
  }
  crop = {
    left: crop.xmin - 0.01,
    top: crop.ymin - 0.01,
    width: crop.xmax - crop.xmin + 0.02,
    height: crop.ymax - crop.ymin + 0.02
  };
  if (!hasCrop) {
    crop = {
      left: 0,
      top: 0,
      width: 1,
      height: 1
    };
  }
  for (var key in crop) {
    if (crop[key] < 0)
      crop[key] = 0;
  }
  if (crop.width <= 0.001)
    crop.width = 1;
  if (crop.height <= 0.001)
    crop.height = 1;

  let cropParams = {
    url: data.url
  };
  let imgStyle = null;
  let calcStyle = (imgWidth, imgHeight) => {
    let pixelCrop = {
      left: crop.left * imgWidth,
      width: crop.width * imgWidth,
      top: crop.top * imgHeight,
      height: crop.height * imgHeight
    };

    let cropRatio = pixelCrop.width / pixelCrop.height;
    let thumbRatio = _thumbWidth / _thumbHeight;
    let scale = null;
    if (cropRatio >= thumbRatio) {
      // Full width.
      scale = _thumbWidth / pixelCrop.width;
    } else {
      // Full height.
      scale = _thumbHeight / pixelCrop.height;
    }
    pixelCrop.left *= scale;
    pixelCrop.top *= scale;
    pixelCrop.width *= scale;
    pixelCrop.height *= scale;
    imgWidth *= scale;
    imgHeight *= scale;

    cropParams.w = _thumbWidth / imgWidth;
    cropParams.h = _thumbHeight / imgHeight;
    let imgStyle = {};
    if (cropRatio >= thumbRatio) {
      // Full width.
      let top = 0;
      if (imgHeight > _thumbHeight) {
        top = (_thumbHeight - pixelCrop.height) / 2 - pixelCrop.top;
        top = top / _thumbHeight;
      }
      imgStyle = {
        top: `${top * 100}%`,
        left: `${-pixelCrop.left / _thumbWidth * 100}%`,
        width: `${imgWidth / _thumbWidth * 100}%`,
        height: `${imgHeight / _thumbHeight * 100}%`,
        position: 'relative',
        visibility: 'visible'
      };
      cropParams.x = pixelCrop.left / imgWidth;
      cropParams.y = -top * _thumbHeight / imgHeight;
    } else {
      // Full height.
      let left = 0;
      if (imgWidth > _thumbWidth) {
        left = (_thumbWidth - pixelCrop.width) / 2 - pixelCrop.left;
        left = left / _thumbWidth;
      }
      imgStyle = {
        top: `${-pixelCrop.top / _thumbHeight * 100}%`,
        left: `${left * 100}%`,
        width: `${imgWidth / _thumbWidth * 100}%`,
        height: `${imgHeight / _thumbHeight * 100}%`,
        position: 'relative',
        visibility: 'visible'
      };
      cropParams.x = -left * _thumbWidth / imgWidth;
      cropParams.y = pixelCrop.top / imgHeight;
    }
    return imgStyle;
  };
  let callback = null;
  if (!data.width || !data.height) {
    imgStyle = {
      position: 'fixed',
      visibility: 'hidden'
    };
    let mark = false;
    callback = event => {
      if (mark) return;
      let imgHeight = event.target.naturalHeight;
      let imgWidth = event.target.naturalWidth;
      imgStyle = calcStyle(imgWidth, imgHeight);
      event.target.parentNode.style = styleObjectToString(imgStyle);
      mark = true;
    }
  } else {
    imgStyle = calcStyle(data.width, data.height);
    let mark = false;
    callback = event => {
      mark = true;
    };
  }
  let rects = genRects(data, params.onRectClick);
  let image = [];
  if (cropParams.w) {
    let partialURL = Utils.cropImage(cropParams);
    image.push(
      <img className="image-tagger-partial-hd" key="hd" src={ partialURL } />
    );
  }
  image.push(
    <div className="image-tagger-image-container" style={ imgStyle } key="div">
      <img
        className="image-tagger-thumbnail-image"
        src={ data.url }
        onLoad={ callback }
      />
      <div
        className="image-tagger-rects-block"
      >
        { rects }
      </div>
    </div>
  );

  return image;
}

function genRects(image, callback) {
  let rects = [];
  image.objs = image.objs || [];
  image.objs.forEach(obj => {
    obj.bndBoxes = obj.bndBoxes || [];
    obj.bndBoxes.forEach(box => {
      box.obj = obj;
      rects.push(box);
    });
  });
  rects.sort((a, b) => {
    let sa = (a.xmax - a.xmin) * (a.ymax - a.ymin);
    let sb = (b.xmax - b.xmin) * (b.ymax - b.ymin);
    return sb - sa;
  });
  rects = rects.map((box, index) => {
    let style = null;
    if (box.xmax && box.ymax) {
      style = {
        top: `${box.ymin * 100}%`,
        left: `${box.xmin * 100}%`,
        width: `${(box.xmax - box.xmin) * 100}%`,
        height: `${(box.ymax - box.ymin) * 100}%`
      };
    } else {
      style = {
        border: 0,
        left: 0,
        top: 0
      };
    }
    let attr = (box.attrs || []).map(attr => {
      let values = [];
      attrOptionForEach(attr.values, v => {
        values.push(v.name);
      });
      return attr.name + '-' + values.join('-');
    }).join(' ');
    let nameText = `${box.obj.name} ${attr}`;
    let handleClick = event => {
      event.stopPropagation();
      if (callback)
        callback(box, box.obj);
    };
    let name = <p>{ nameText }</p>;
    let width = box.xmax - box.xmin;
    let height = box.ymax - box.ymin;
    return (
      <div
        key={ box.uid || index }
        className="image-tagger-tag-rect"
        style={ style }
        onClick={ handleClick }
        title={ nameText }
      >
        { name }
      </div>
    );
    });
  return rects;
}

class RectDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: null,
      rect: null,
      show: false,
      offset: 'left',
      thumbWidth: 0,
      thumbHeight: 0,
      imgWidth: 0,
      imgHeight: 0
    };
  }
  show(url, rect, params) {
    this.setState({
      url: url,
      rect: rect,
      show: true,
      offset: params.offset,
      thumbWidth: params.thumbWidth,
      thumbHeight: params.thumbHeight,
      imgWidth: params.imgWidth,
      imgHeight: params.imgHeight
    });
  }
  hide() {
    this.setState({show: false});
  }
  render() {
    let imgElem = null;
    if (this.state.rect) {
      let data = {
        'url': this.state.url,
        'width': this.state.imgWidth,
        'height': this.state.imgHeight,
        'objs': [{
          'bndBoxes': [this.state.rect],
          'uid': 1,
          'name': this.state.rect && this.state.rect.name
        }]
      };
      imgElem = genImageElem(data, this.state.thumbWidth, this.state.thumbHeight);
    }
    let style = {
      'display': this.state.show ? 'block' : 'none',
      'width': `${this.state.thumbWidth}px`,
      'height': `${this.state.thumbHeight}px`,
      'left': `${this.state.offset}px`
    };
    return (
      <div className="image-tagger-rect-detail-container" style={ style }>
        { imgElem }
      </div>
    );
  }
}

export { RectDetail as default };

Image Tagger
=============

## Rect Notation 

```
xmin = x
ymin = y
xmax = x + w
ymax = y + h
```
This rule applies whether values are scale or pixels measured. `x`, `y`, `w`, `h`'s definitions are same with css box model. Thus `xmin`-serial is zero-indexed.

## Initialize

Refer bootstrap's css file in `head` tag. Then import `image-tagger-vendor.js` and `image-tagger-bundle.js`, the former packs all dependencies.
After all of that, call `imageTaggerSetup` to initialize tagger:

```javascript
imageTaggerSetup(
  'image-tagger-wrapper', // container's id
  'http://192.168.1.183:7334/image', // api url
  0, // taskId
  0x1f, // control tools select, default 0x1f
  {'rectZoom': true}, // other params,
  callback
);
```

## Data format

Tagger `GET` the api url with task id and sequence id to get image data. Server should return a JSON doc.

`optype` can be `"tag"` or `"classify"`.
`type` of attribute *must* more than 0 and less than 10.

Use `crop` field to specify the area to tag rects on.

```json
{
  "errcode":0,
  "unitid":3267940,
  "imgid":"11425736066558964286",
  "maxseqid":50,
  "url":"http://211.157.161.43/img005/286/9e905e6a989db23e.jpg",
  "description":"",
  "src":"",
  "optype": "tag",
  "angle": 90,
  "originalSize": true,
  "objs":[
    {
      "name":"连衣裙",
      "uid":42341,
      "geometry":"bndbox",
      "bndBoxes":[
        {
          "editable":false,
          "uid": 70,
          "xmin":0.2191726525351,
          "ymin":0.12427184466019,
          "xmax":0.90455938400231,
          "ymax":0.59902912621359,
          "stroke":"black"
        }
      ],
      "attrs":[
        {
          "uid":0,
          "unique": true,
          "required": true,
          "type": 2,
          "display": true,
          "name":"裙型",
          "options":[
            {
              "name":"Option 0",
              "uid":0
            }
          ]
        },
        {
          "uid":1,
          "name":"颜色",
          "options":[
            {
              "name":"红",
              "uid":0
            },
            {
              "name":"黄",
              "uid":1
            },
            {
              "name":"蓝",
              "uid":2
            }
          ]
        }
      ]
    }
  ]
}
```

Tagger send post request to save user's tagging every time the submit button is clicked.

NOTE: front end script abandon old sequence whenever new sequence is seeked. It depends on the server to save all the changes.

The sequence id and task id is encoded in the post url. The POST body lookes like the data provide by the server except few changes. Response to `POST` request is expected to contain `maxseqid`.

`operation` can be any of `"skip"` or `"alter"`.

```json
{
  "unitid": 3267940,
  "url": "http://211.157.161.43/img002/51/b5b89bb7932845e3.jpg",
  "imgid": "11425736066558964286",
  "operation": "alter",
  "angle": 90,
  "objs": [
    {
      "name": "连衣裙",
      "uid": 42341,
      "geometry": "bndbox",
      "bndBoxes": [
        {
          "xmin": 63.234356194361595,
          "ymin": 193.39999389648438,
          "xmax": 355.776550669111,
          "ymax": 478.3999938964844,
          "attrs": [
            {
              "uid": 0,
              "name": "裙型",
              "values": [
                {
                  "name": "C",
                  "uid": 2
                },
                {
                  "name": "E",
                  "uid": 4
                }
              ]
            },
            {
              "uid": 1,
              "name": "颜色",
              "values": [
                {
                  "name": "红",
                  "uid": 0
                }
              ]
            }
          ],
          "stroke": "#DC143C"
        }
      ]
    },
    {
      "name": "新裙子",
      "uid": 42342,
      "geometry": "bndbox",
      "bndBoxes": []
    }
  ]
}
```

## Error Code

15 Authentication expired
9 Updating succeeded

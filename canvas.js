import React from 'react';
import { fabric } from 'fabric';
import { CropRect } from './crop-mode.js';
import ClickNDragCrop from './drag-crop-mode.js';
import ClickCrop from './click-crop-mode.js';
import RectDetail from './detail-dialog.js';
import Slider from 'react-rangeslider'

import 'react-rangeslider/lib/index.css'

var unsupportAlert = "您的浏览器不支持画布";
fabric.Object.prototype.cornerSize = 6;
fabric.Object.prototype.borderColor = '#505050';
fabric.Object.prototype.transparentCorners = false;
fabric.Object.prototype.lockRotation = true;
fabric.Object.prototype.hasRotatingPoint = false;
fabric.Object.prototype.objectCaching = false;

class Canvas extends React.Component {
  constructor(props) {
    super(props);
    // `props.onCropSelected`
    // `props.onPageInc`
    this._canvas = null;
    this._containerElem = null;
    this._currentMode = null;

    this._handleCropSelected = this._handleCropSelected.bind(this);
    this._setFooterOffset = this._setFooterOffset.bind(this);
    this._handleCropHover = this._handleCropHover.bind(this);
    this._handleCropOut = this._handleCropOut.bind(this);
    this._resumeControlPannel = this._resumeControlPannel.bind(this);
    this._controlPannelZoomMode = this._controlPannelZoomMode.bind(this);
    this._triggleSelect = this._triggleSelect.bind(this);

    let maxHeightSpan = this.props.maxHeightSpan || 600;

    this.state = {
      canvasLeft: 0,
      canvasTop: 0,
      containerWidth: null,
      containerHeight: null,
      originalSize: false
    };
  }
  get cropMode() {
    return this._cropMode;
  }
  resumeProps(oldMode, newMode) {
    newMode.imageParams = oldMode.imageParams || '';
    newMode.name = oldMode.name || '';
    newMode._zoomLevel = oldMode._zoomLevel;
    newMode._imageElement = oldMode._imageElement;
    newMode.imageURL = oldMode.imageURL;
    newMode._naturalWidth = oldMode._naturalWidth;
    newMode._naturalHeight = oldMode._naturalHeight;
    if (oldMode._workMode == 'zoom-mode') {
      oldMode._afterZoomMode();
      newMode.zoomMode();
    }
  }
  changeMode(mode) {
    if (this._currentMode === mode)
      return;
    this._cropMode._deactivateAll();
    var rects = null;
    var oldMode = this._cropMode;
    if (this._cropMode) {
      rects = this._cropMode.getRects();
    }
    switch (mode) {
      case 'click':
        this._cropMode = new ClickCrop(this._canvas, {
          maxHeightSpan: this.props.maxHeightSpan
        });
        this._currentMode = 'click';
        break;
      case 'drag':
        this._cropMode = new ClickNDragCrop(this._canvas, {
          maxHeightSpan: this.props.maxHeightSpan
        });
        this._currentMode = 'drag';        
        break;
    }
    this.resumeProps(oldMode, this._cropMode);
    this._cropMode.addCrops(rects, ['attrs']);
    this._attachModeCallbacks();
    oldMode.cleanup();
    this._cropMode.tagMode();
  }
  _attachModeCallbacks(cropMode) {
    this._cropMode.setFooterOffset = this._setFooterOffset;
    this._cropMode.onCropSelected = this._handleCropSelected;
    this._cropMode.controlPannelZoomMode = this._controlPannelZoomMode;
    if (!(this.props.tools & 0x03)) {
      this._cropMode.afterZoomMode = this._triggleSelect;
    }
  }
  _triggleSelect() {
    this.props.triggleCommand('select');
  }
  _setFooterOffset(offset) {
    this.props.setFooterOffset(offset);
  }
  _controlPannelZoomMode() {
    this.props.triggleZoomMode();
    this._resumeControlPannel();
  }
  _resumeControlPannel() {
    switch (this._currentMode) {
      case 'drag':
        this.props.triggleCommand('drag-tag', true);
        break;
      case 'click':
        this.props.triggleCommand('dot-tag', true);
        break;
    }
  }
  _handlePageInc(step) {
    if (this.props.onPageInc)
      this.props.onPageInc(step);
  }
  _handleCropSelected(crop) {
    if (this.props.onCropSelected)
      this.props.onCropSelected(crop);
  }
  _getDetailParam(crop) {
    let thumbSpan = this._canvas.getWidth() * 2 / 5;
    let threshold = thumbSpan < 100 ? thumbSpan : 100;
    if (crop && (crop.width < threshold || crop.height < threshold)) {
      let params = {
        thumbWidth: thumbSpan,
        thumbHeight: this._canvas.getHeight() * 2 / 5,
        offset: (crop.left + crop.width) > thumbSpan ? 0 :
                this._canvas.getWidth() - thumbSpan,
        imgWidth: this._cropMode._naturalWidth,
        imgHeight: this._cropMode._naturalHeight,
      };
      return params;
    }
    return null;
  }
  _handleCropHover(e) {
    if (!this.props.rectZoom || this._cropMode._workMode !== 'select-mode')
      return;
    let crop = e.target;
    let params = this._getDetailParam(crop);
    if (params) {
      this.refs.rectDetail.show(this._cropMode.imageURL, {
        xmin: crop.left / this._canvas.getWidth(),
        xmax: (crop.left + crop.width) / this._canvas.getWidth(),
        ymin: crop.top / this._canvas.getHeight(),
        ymax: (crop.top + crop.height) / this._canvas.getHeight(),
        name: crop.name
      }, params);
    }
  }
  _handleCropOut(e) {
    if (!this.props.rectZoom || this._cropMode._workMode !== 'select-mode')
      return;
    let crop = e.target;
    let params = this._getDetailParam(crop);
    if (params) {
      this.refs.rectDetail.hide();
    }
  }
  _handleObjectMoving(e){
    var zoomLevel = this._cropMode._zoomLevel;
    var obj = e.target;
    if (obj.get('type') !== 'CropRect') {
      return;
    }
    if (obj.currentHeight > obj.canvas.height ||
        obj.currentWidth > obj.canvas.width) {
      return;
    }
    obj.setCoords();
    // top-left corner
    if (obj.top < -obj.strokeWidth) {
      obj.top = -obj.strokeWidth;
    }
    if (obj.left < -obj.strokeWidth) {
      obj.left = -obj.strokeWidth;
    }
    if (obj.top + obj.height > obj.canvas.height) {
      obj.top = obj.canvas.height - obj.height;
    }
    if (obj.left + obj.width > obj.canvas.width) {
      obj.left = obj.canvas.width - obj.width;
    }
  }
  componentDidMount() {
    var container = document.getElementById('image-tagger-picture-container');
    this._containerElem = container;
    this._canvas = new fabric.Canvas('image-tagger-canvas', {
      height: container.clientHeight,
      width: container.clientWidth
    });
    this._canvas.on('mouse:over', this._handleCropHover);
    this._canvas.on('mouse:out', this._handleCropOut);
    this._canvas.on('object:moving', this._handleObjectMoving.bind(this));
    this._cropMode = new ClickNDragCrop(this._canvas, {
      maxHeightSpan: this.props.maxHeightSpan
    });
    this._cropMode.tagMode();
    this._attachModeCallbacks();
  }
  render() {
    let maxHeightSpan = this.props.maxHeightSpan || 600;
    let containerStyle = null;
    if (!this.state.originalSize) {
      containerStyle = {
        height: `${maxHeightSpan}px`,
        right: maxHeightSpan == 600 ? '25%' : '35%'
      };
      if (this.state.containerWidth)
        containerStyle.width = `${this.state.containerWidth}px`;
      if (this.state.containerHeight)
        containerStyle.height = `${this.state.containerHeight}px`;
    } else {
      containerStyle = {
        left: 0,
        right: 0
      };
    }
    let canvasStyle= {
      left: `${this.state.canvasLeft || 0}px`,
      top: `${this.state.canvasTop || 0}px`,
    };
    return (
      <div id="image-tagger-picture-container" style={ containerStyle }>
        <canvas
          style={ canvasStyle }
          id="image-tagger-canvas"
        >{ unsupportAlert }</canvas>
        <RectDetail ref="rectDetail" />
      </div>
    );
  }
}

export { Canvas as default };

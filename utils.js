import $ from 'jquery';
import querystring from 'querystring';

//********************************************************************
// extend a with b
//********************************************************************
function extendDict(a, b) {
  var result = {};
  for (var prop in a) {
    result[prop] = a[prop];
  }
  for (var prop in b) {
    if (!result.hasOwnProperty(prop))
      result[prop] = b[prop];
  }
  return result;
}

function max(a, b) {
  if (b > a)
    return b;
  return a;
}

function min(a, b) {
  if (b < a)
    return b;
  return a;
}

function requestForImage(url, callback) {
  $.ajax({
    url: url,
    dataType: 'json',
    timeout: 2000,
    success: function (data) {
      if (callback)
        callback(null, data);
    },
    error: function (xhr, status, err) {
      console.error('ImageTaggerError: requestForImage, ', err);
      if (callback)
        callback(err);
      else
        throw err;
    }
  });
}

//********************************************************************
// postJSONPromise
// @param url
// @param json data
//********************************************************************
function postJSONPromise (url, jsonData) {
    var callback = function (resolve, reject) {
        var xhr;
        var stripData = function () {
            try {
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                reject(e);
            }
            resolve(data);
        };

        if (typeof XDomainRequest !== 'undefined') {
            xhr = new XDomainRequest();
            xhr.onload = stripData;
            xhr.onerror = function () {
                reject(new Error(xhr.statusText));
            };
        } else {
            xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState !== 4)
                    return;
                if (xhr.status === 200) {
                    stripData();
                } else {
                    reject(new Error(xhr.statusText));
                }
            };
        }
        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/json");
        if (jsonData) {
            var data = typeof jsonData === "string" ? jsonData :
                JSON.stringify(jsonData);
            xhr.send(data);
        } else {
            xhr.send();
        }
    };
    return new Promise(callback);
}

function rgbColor(color) {
  var d = document.createElement('div');
  d.style.color = color;
  d.style.display = 'none';
  document.body.appendChild(d);
  var hColor = window.getComputedStyle(d).color;
  d.remove();
  return hColor;
}

function invertRGB(color) {
  if (!color.startsWith('rgb(')) {
    throw new Error(`invalid color ${color}`);
  }
  color = color.slice(4, -1);
  color = color.split(',');
  if (color.length !== 3)
    throw new Error('invalid color length');
  var r = (255 - parseInt(color[0])).toString(16),
      g = (255 - parseInt(color[1])).toString(16),
      b = (255 - parseInt(color[2])).toString(16);
  // pad each with zeros and return
  return '#' + padZero(r) + padZero(g) + padZero(b);
}

function padZero(str, len) {
  len = len || 2;
  var zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
}

function cropImage(query) {
  var prefix = 'http://its.adkalava.com/admin/imgCut';
  var url = `${prefix}?${querystring.encode(query)}`;
  return url;
}

function isNumber(n) {
  return typeof n == 'number' && !isNaN(n - n);
}

export {
  extendDict, max, min, rgbColor, cropImage,
  requestForImage, postJSONPromise, invertRGB, isNumber
};

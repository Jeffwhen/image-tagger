import React from 'react';
import ReactDOM from 'react-dom';
import * as Bootstrap from 'react-bootstrap';
import * as Utils from './utils.js';
import mousetrap from 'mousetrap';
import Alert from 'react-s-alert';
import url from  'url';
const querystring = require('querystring');

require('./image-tagger.css');
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/genie.css';

import { postJSONPromise } from './utils.js';
import ControlPannel from './control-pannel.js';
import PictureEdit from './canvas.js';
import Pagination from './pagination.js';
import AttrsBox from './attrs-box.js';
import ImageDataValidate from './validate.js';

window.imageTaggerSetup = initialize;

function initialize(id, url, taskId, tools, params, callback) {
  if (!id) {
    throw new Error('Container id needed.');
    return;
  }
  if (!url || typeof taskId !== 'number') {
    throw new Error('ImageTaggerWarning: image URL or taskId is empty.');
    return;
  }
  var seqId = Math.floor(window.location.hash.slice(1));
  if (Number.isNaN(seqId))
    seqId = 0;
  var wrapper = document.getElementById(id);
  var winHeight = window.innerHeight
               || document.documentElement.clientHeight
               || document.body.clientHeight;
  var defaultSpan = winHeight > 850 ? 600 : 400;
  var rootComponent = ReactDOM.render(
    <ImageTagger
      taskId={taskId}
      url={ url }
      seqId={ seqId }
      tools={ tools }
      maxHeightSpan={ params && params.maxHeightSpan || defaultSpan }
      rectZoom={ params && params.rectZoom }
    />,
    wrapper
  );
}

class ConfirmBox extends React.Component {
  constructor(props) {
    // `props.onConfirm`
    // `props.onCancel`
    // `props.body`
    // `props.title`
    super(props);
    this.state = {
      showModal: false
    };
  }
  show() {
    this.state.showModal = true;
    this.forceUpdate();
  }
  _handleConfirm() {
    this.state.showModal = false;
    this.forceUpdate();
    if (this.props.onConfirm)
      this.props.onConfirm();
  }
  _handleCancel() {
    this.state.showModal = false;
    this.forceUpdate();
    if (this.props.onCancel)
      this.props.onCancel();
  }
  _createCancelButton() {
    return this.props.hideCancel ? [] : (
      <Bootstrap.Button onClick={ this._handleCancel.bind(this)}
      >取消</Bootstrap.Button>
    )
  }
  render() {
    return (
      <Bootstrap.Modal show={ this.state.showModal } bsSize="small">
      <Bootstrap.Modal.Header>
      <Bootstrap.Modal.Title>{ this.props.title }</Bootstrap.Modal.Title>
      </Bootstrap.Modal.Header>
      <Bootstrap.Modal.Body>
      { this.props.body }
      </Bootstrap.Modal.Body>
      <Bootstrap.Modal.Footer>
      { this._createCancelButton() }
      <Bootstrap.Button
      onClick={ this._handleConfirm.bind(this) }
      bsStyle="primary">确定</Bootstrap.Button>
      </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

class ImageTagger extends React.Component {
  constructor(props) {
    super(props);
    // `props.url`
    // `props.taskId`
    // `props.seqId`
    // `props.tools`
    this._url = this.props.url;
    this._taskId = this.props.taskId;
    this._seqId = this.props.seqId || 0;
    this._data = null;
    this._mutateStatus = false;
    this.state = {};

    this.state.footerOffset = null;

    // Components will be bind after mount.
    this._pagination = null;
    this._canvas = null;
    this._attrsBox = null;
    this._config = null;

    this._confirmDialog = this._dialog;
    this._alert = this._errorDialog;
    this.tagTool = 'drag-tag';

    // Key binding.
    this.suspending = false;
    for (let i = 0; i < 9; i++) {
      mousetrap.bind(
        `${i + 1}`,
        this.selectObject.bind(this, i),
        'keyup'
      );
    }
    mousetrap.bind(
      `a`,
      this.setIndex.bind(this, 1),
      'keyup'
    );
    mousetrap.bind(
      `f`,
      this.endIndex.bind(this),
      'keyup'
    );
    mousetrap.bind(
      `esc`,
      this.escape.bind(this),
      'keyup'
    );

    if (this.props.tools & 0x01) {
      mousetrap.bind(
        'q',
        this.triggleCommand.bind(this, 'drag-tag'),
        'keyup'
      );
    }
    if (this.props.tools & 0x02) {
      mousetrap.bind(
        'w',
        this.triggleCommand.bind(this, 'dot-tag'),
        'keyup'
      );
    }
    if (this.props.tools & 0x04) {
      mousetrap.bind(
        'e',
        this.triggleCommand.bind(this, 'select'),
        'keyup'
      );
    }
    if (this.props.tools & 0x08) {
      mousetrap.bind(
        'r',
        this.triggleCommand.bind(this, 'select-all'),
        'keyup'
      );
    }
    if (this.props.tools & 0x10) {
      mousetrap.bind(
        'd',
        this.triggleCommand.bind(this, 'remove'),
        'keyup'
      );
    }
    if (this.props.tools & 0x20) {
      mousetrap.bind(
        `t`,
        this.triggleCommand.bind(this, 'zoom')
      );
    }
    if (this.props.tools & 0x40) {
      mousetrap.bind(
        `g`,
        this._rotate.bind(this)
      );
    }

    mousetrap.bind(
      's',
      this._handleEnterKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'z',
      this._handleLeftKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'x',
      this._handleRightKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'c',
      this._nextCrop.bind(this, -1),
      'keyup'
    );
    mousetrap.bind(
      'v',
      this._nextCrop.bind(this, 1),
      'keyup'
    );
    mousetrap.bind(
      'enter',
      this._handleEnterKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'enter',
      this._preventDefault.bind(this)
    );
    mousetrap.bind(
      'left',
      this._handleLeftKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'right',
      this._handleRightKeydown.bind(this),
      'keyup'
    );
  }
  endIndex() {
    this.setIndex(this._data.maxseqid + 1);
  }
  setIndex(n) {
    this._pagination._handleSelect(n);
  }
  escape() {
    this._canvas.cropMode.clearOthers();
  }
  _preventDefault(event) {
    if (event.target.tagName == 'INPUT') {
      event.target.blur();
      return;
    }
    event.preventDefault();
  }
  _handleLeftKeydown() {
    if (this.suspending)
      return;
    this._handlePageIndicatorClicked(-1);
  }
  _handleRightKeydown() {
    if (this.suspending)
      return;
    this._handlePageIndicatorClicked(1);
  }
  _handleEnterKeydown(event) {
    if (event.target.tagName == 'INPUT') {
      event.target.blur();
      return;
    }
    if (this.suspending)
      return;
    this._handleSubmit();
  }
  selectObject(index) {
    this._attrsBox._handleObjectSelect(index);
  }
  get URLWithQuery() {
    var preQuery = url.parse(window.location.href).query;
    preQuery = querystring.parse(preQuery);
    preQuery.taskid = this._taskId;
    preQuery.seqid = this._seqId;
    var urlObj = url.parse(this._url);
    urlObj.query = preQuery;
    return url.format(urlObj);
  }
  _dialog(title, body, confirmCallback, cancelCallback, hideCancel) {
    this.suspending = true;
    let confirm = () => {
      this.suspending = false;
      if (confirmCallback)
        confirmCallback();
    };
    let cancel = () => {
      this.suspending = false;
      if (cancelCallback)
        cancelCallback();
    };
    var box = ReactDOM.render(
      <ConfirmBox
        body={ body }
        title={ title }
        onConfirm={ confirm }
        onCancel={ cancel }
        hideCancel={ hideCancel }
      />,
      document.getElementById('image-tagger-modal')
    );
    box.show();
  }
  _errorDialog(title, body) {
    this._dialog(title, body, null, null, true);
  }
  _updateImageData(data, callback) {
    this.hasUpdated = false;
    if (!ImageDataValidate(data)) {
      Alert.error('获取图片失败: 服务器返回数据格式错误', {
        position: 'bottom-left',
        effect: 'genie'
      });
      return;
    }
    this._data = data;
    // Reference info.
    this.state.referURL = data.src;
    this.state.description = data.description;

    // Set pagination to right place.
    this._pagination.length = data.maxseqid + 1;
    this._pagination.activePage = this._seqId + 1;
    window.location.hash = '#' + this._seqId;

    // Update image.
    this._canvas.cropMode.clearCanvas();
    var params = {originalSize: data.originalSize};
    this._canvas.setState(params);
    delete this._canvas.cropMode.imageParams;
    params.angle = data.angle;
    this._canvas.cropMode.updateImage(data.url, params, () => {
      // Restore rects.
      data.objs.forEach(object => {
        this._canvas.cropMode.name = object.name;
        if (!object.bndBoxes)
          object.rects = [];
        else {
          object.rects = object.bndBoxes.map((box, index) => {
            if (!box.attrs)
              box.attrs = [];
            for (var i in box.attrs) {
              var attr = box.attrs[i];
              var objAttr = object.attrs.filter(objectAttr => {
                return objectAttr.uid === attr.uid;
              })[0];
              if (objAttr && objAttr.type !== undefined) {
                attr.type = objAttr.type;
                console.assert(0 < attr.type && attr.type < 10);
                attr.uid = Number(String(attr.uid) + attr.type);
              } else if (objAttr) {
                attr.uid = attr.uid * 10;
              }
              attr.display = objAttr && objAttr.display;
              if (attr.display === undefined)
                attr.display = true;
              attr.unique = objAttr && objAttr.unique;
              if (attr.unique === undefined)
                attr.unique = true;
              if (attr.required === undefined)
                attr.required = true;
            }
            box.description = this._getDescriptionFromAttrList(box.attrs);
            if (box.uid) {
              box.uid = box.uid;
              box.upstream = true;
            }
            box.objectId = object.uid;
            box.top = box.ymin;
            box.left = box.xmin;
            box.height = box.ymax - box.ymin;
            box.width = box.xmax - box.xmin;
            delete box.xmax;
            delete box.xmin;
            delete box.ymax;
            delete box.ymin;
            delete box.name;
            return box;
          });
        }
        this._canvas.cropMode.addCrops(
          object.rects,
          ['boxId']
        );
        object.attrs.forEach(objAttr => {
          if (objAttr.display === undefined)
            objAttr.display = true;
          if (objAttr.required === undefined)
            objAttr.required = true;
          if (objAttr.unique === undefined)
            objAttr.unique = true;
          objAttr.uid = Number(
            String(objAttr.uid) + Number(objAttr.type || 0)
          );
        });
        delete object.bndBoxes;
      });

      if (this._zoomMode) {
        this._exitZoomMode();
      }

      // Update objects and their attributes.
      this._attrsBox.update(data.objs);

      let crops = this._canvas.cropMode.getCrops();
      if (!(this.props.tools & 0x03) && crops.length === 1) {
        let crop = crops[0];
        this._canvas.cropMode.setActiveCrop(crop.boxIndex);
        let increment = 1 / 6;
        let width = crop.width + crop.width * increment;
        let height = crop.height + crop.height * increment;
        let threshold = 0.6;
        let canvasWidth = this._canvas.cropMode._canvas.getWidth();
        let canvasHeight = this._canvas.cropMode._canvas.getHeight();
        if (width < threshold * canvasWidth &&
            height < threshold * canvasHeight) {
          this._canvas.cropMode.rectZoom({
            left: crop.left - crop.width * increment / 2,
            top: crop.top - crop.height * increment / 2,
            width: width,
            height: height
          });
        }
      }

      if (callback)
        callback();
    });
    this._canvas.cropMode.refresh();
    this.forceUpdate();
    this._mutateStatus = false;
  }
  _updateCurrentSlice(callback) {
    this._canvas.cropMode.clearCanvas();
    Utils.requestForImage(this.URLWithQuery, (
      err, data
    ) => {
      if (err || !data || data.errcode !== 0) {
        var err = err ? JSON.stringify(err) : '';
        var msg = `${data.errmsg}\n${err}`;
        Alert.error(`获取图片失败: ${msg}`, {
          position: 'bottom-left',
          effect: 'genie'
        });
        if (data.errcode == 15) {
          window.location.reload();
        } else {
          return;
        }
      }
      this._updateImageData(data, callback);
    });
  }
  // Event handler for `controlPannel`
  _handleCommand(options) {
    // TODO
    if (options.id === 'select-tool' &&
        options.selected.value === 'select') {
      this._canvas.cropMode.selectMode();
    } else if (options.id === 'tag-tool' &&
               options.selected.value === 'drag-tag') {
      this._canvas.changeMode('drag');
      this.tagTool = 'drag-tag';
      this._canvas.cropMode.tagMode();
      this._canvas.cropMode.stroke = this._config.getColor();
    } else if (options.id === 'tag-tool' &&
               options.selected.value === 'dot-tag') {
      this.tagTool = 'dot-tag';
      this._canvas.changeMode('click');
      this._canvas.cropMode.tagMode();
      this._canvas.cropMode.stroke = this._config.getColor();
    } else if (options.id === 'select-tool' &&
               options.selected.value === 'remove') {
      this._canvas.cropMode.removeCrops();
      if (!this._canvas.cropMode.getCrops().length)
        this.triggleCommand(this.tagTool || 'drag-tag');
      this._canvas.cropMode.refresh();
    } else if (options.id === 'select-tool' &&
               options.selected.value === 'select-all') {
      this._canvas.cropMode.selectMode();
      this._canvas.cropMode.setActiveCrops(
        this._canvas.cropMode.getCrops().map(crop => {
          return crop.boxIndex;
        })
      );
      this._canvas.cropMode.refresh();
    } else if (options.id === 'zoom' &&
               options.selected.value === 'zoom') {
      if (!this._zoomMode) {
        this._canvas.cropMode.zoomMode();
      } else {
        this._exitZoomMode();
      }
    } else if (options.id === 'rotate' &&
               options.selected.value === 'rotate') {
      this._rotate();
    } else if (options.id === 'color') {
      this._canvas.cropMode.stroke = options.selected.value;
      this._canvas.cropMode.refresh();
    }
  }
  _rotate() {
    this._canvas.cropMode.rotate();
  }
  _exitZoomMode() {
    this._canvas.cropMode.exitZoom();
    this._zoomMode = false;
    this.triggleZoomMode(true);
    this._canvas._resumeControlPannel();
    if (!(this.props.tools & 0x03)) {
      this.triggleCommand('select');
    }
  }
  // Event handler for `canvas`
  // TODO group
  _handleCropSelected(crop) {
    if (typeof crop.objectId !== 'number') {
      crop.objectId = this._attrsBox.getSelectedObject().uid;
    }
    else {
      // This expression DOES NOT triggle selectedObject change event.
      this._attrsBox.setSelectedObject(crop.objectId);
    }
    if (!(crop.attrs instanceof Array)) {
      crop.attrs = [];
    }
    this._attrsBox.setSelectedAttrs(crop.attrs);
  }
  // Event handler for `attrsBox`.
  _handleSelectedObjectChange(selectedObject) {
    this._canvas.cropMode.name = selectedObject.name;
    this._canvas.cropMode.objectId = selectedObject.uid;
    this._canvas.cropMode.refresh();
  }
  _getDescriptionFromAttrList(attrs) {
    var description = '';
    for (var i in attrs) {
      var attr = attrs[i];
      if (!attr.display)
        continue;
      attr.values = attr.values || []
      attr.values.forEach(value => {
        description += value.name + '&';
      });
      description = description.slice(0, -1);
      description += ' ';
    }
    description = description.slice(0, -1);
    return description;
  }
  // Event handler for `attrsBox`.
  _handleAttrChange(attrs) {
    var description = this._getDescriptionFromAttrList(attrs);
    try {
      this._canvas.cropMode.attrs = attrs;
    } catch (e) {
      Alert.error('更新属性失败: ' + e.message, {
        position: 'bottom-left',
        effect: 'genie'
      });
      return;
    }
    this._canvas.cropMode.description = description;

    let crop = this._canvas.cropMode.getActiveCrops();
    crop = crop && crop[0];
    let obj = this._attrsBox.getSelectedObject();
    if ((crop && crop.attrs.length || 0) >= obj.attrs.length) {
      let next = null;
      for (let i = 0; i < this._canvas.cropMode._crops.length; i++) {
        let c = this._canvas.cropMode._crops[i];
        if (!c)
          continue
        let filled = c.attrs.filter(x => {return x.values.length;});
        if (filled.length < obj.attrs.length) {
          next = c;
          break;
        }
      }
      if (next)
        this._canvas.cropMode.setActiveCrop(next.boxIndex);
    }

    this._canvas.cropMode.refresh();
  }
  _nextCrop(step) {
    if (!this._canvas.cropMode._crops.length)
      return;
    step = step || 1;
    let crop = this._canvas.cropMode.getActiveCrops();
    crop = crop && crop[crop.length - 1];

    let next = null;
    if (!crop) {
      next = 0;
    } else {
      let prolonged = crop.boxIndex;
      let crops = this._canvas.cropMode._crops;
      do {
        prolonged = prolonged + step;
        next = prolonged % crops.length;
        if (next < 0)
          next += crops.length;
      } while (Math.abs(prolonged) < crops.length * 2 && !crops[next]);
    }
    this._canvas.cropMode.setActiveCrop(next);
    this._canvas.cropMode.refresh();
  }
  _handlePageIndicatorClicked(step) {
    if (!this._hasChanged()) {
      this._pagination.incKey(step);
      return;
    }
    var confirmCallback = () => {
      this._pagination.incKey(step);
    };
    this._confirmDialog(
      '放弃编辑?',
      '如需保存请点击提交按钮',
      confirmCallback
    );
  }
  _handleBeforePageChange() {
    return true;
  }
  // Event handler for `pagination`
  _handlePageChange(eventKey, callback) {
    this._seqId = eventKey - 1;
    this._updateCurrentSlice(callback);
  }
  _handleSubmit(callback) {
    if (this._submitDone === false)
      return callback && callback();
    var newCallback = () => {
      this._submitDone = true;
      callback && callback();
    };
    this._submitDone = false;
    this.escape();
    this._submit((data) => {
      if (data && data.errcode) {
        if (data.errcode == 9)
          this.hasUpdated = true;
        Alert.info(
          `消息: ${data.errmsg || ''}`, {
            position: 'bottom-left',
            effect: 'genie'
          }
        );
        if (data.errcode == 15) {
          window.location.reload();
        }
        newCallback();
      }
      else if (data) {
        this._pagination.length = data.maxseqid + 1;
        this._seqId = this._pagination.state.activePage;
        this._updateImageData(data, newCallback);
      } else {
        newCallback();
      }
    });
  }
  _handleSkip(callback) {
    this._skip((data) => {
      if (data && data.errcode)
        this._alert(
          '消息',
          data.errmsg || ''
        );
      else if (data) {
        this._pagination.length = data.maxseqid + 1;
        this._pagination.incKey(1);
      }
      if (callback)
        callback();
    });
  }
  _rectEqual(a, b) {
    if (a === b) return true;
    if (!a || !b) return false;
    var isEqual = true;
    if (JSON.stringify(a.attrs) !== JSON.stringify(b.attrs))
      isEqual = false;
    var samePos = a.top === b.top && a.left === b.left &&
                  a.width === b.width && a.height === b.height;
    if (!samePos)
      isEqual = false;
    return isEqual;
  }
  // Check if rects has chenged.
  _hasChanged() {
    var imgParams = this._canvas.cropMode.imageParams;
    var angle = 0;
    var origAngle = this._data.angle || 0;
    if (imgParams && imgParams.angle) {
      angle = imgParams.angle;
    }
    if (this.hasUpdated)
      return false;
    if (angle !== origAngle)
      return true;
    if (this._mutateStatus)
      return true;
    var crops = this._canvas.cropMode.getCrops();
    var origRectNum = 0;
    this._data.objs.forEach(object => {
      origRectNum += object.rects.length;
    });
    if (crops.length !== origRectNum) {
      this._mutateStatus = true;
      return true;
    }
    var hasChanged = false;
    for (var i in crops) {
      var crop = crops[i];
      var object = this._data.objs.find((object) => {
        return object.uid === crop.objectId;
      });
      if (!object) return;
      var rect = object.rects.find((rect) => {
        if (crop.uid) {
          return crop.uid === rect.uid;
        }
        let cropId = `${crop.top}-${crop.left}-${crop.height}-${crop.width}`;
        let rectId = `${rect.top}-${rect.left}-${rect.height}-${rect.width}`;
        return cropId === rectId;
      });
      if (!rect || !this._rectEqual(rect, crop)) {
        hasChanged = true;
        break;
      }
    }
    this._mutateStatus = hasChanged;
    return hasChanged;
  }
  _skip(callback) {
    var postData = {
      imgid: this._data.imgid,
      unitid: this._data.unitid,
      url: this._data.url,
      operation: 'skip'
    };
    postJSONPromise(this.URLWithQuery, postData).then(data => {
      this.hasChanged = false;
      if (callback)
        callback(data);
    });
  }
  _submit(callback) {
    let dataObjs = this._data.objs.map(object => {
      return {
        attrs: object.attrs,
        name: object.name,
        rects: [],
        uid: object.uid
      };
    });
    var lackAttr = false;
    this._canvas.cropMode.getCrops().forEach(rect => {
      var object = dataObjs.find(object => {
        return object.uid === rect.objectId;
      });
      object.attrs.forEach(attr => {
        if (!attr.required)
          return;
        var rectAttrs = rect.attrs || [];
        var rectHasAttr = rectAttrs.filter(rectAttr => {
          return rectAttr.uid === attr.uid;
        });
        if (!rectHasAttr.length) {
          lackAttr = true;
        }
      });
      try {
        rect = {
          objectId: rect.objectId,
          uid: rect.uid,
          stroke: rect.stroke,
          left: rect.left,
          top: rect.top,
          width: rect.width,
          height: rect.height,
          upstream: rect.upstream,
          attrs: JSON.parse(JSON.stringify(rect.attrs))
        };
        rect.attrs.forEach(attr => {
          // To compatible with type in order to handle
          // embedded attribute.
          let attrId = Math.floor(attr.uid / 10);
          attr.uid = attrId;
          if (attr.required && !attr.values.length) {
            lackAttr = true;
          }
        });
        if (!lackAttr)
          object.rects.push(rect);
      } catch (error) {
        console.error('ImageTaggerError: rect\'s objectId not valid!');
        throw error;
      }
    }); // `dataObjs` is updated now.
    if (lackAttr) {
      Alert.error('必须属性未标注！请检查', {
        position: 'bottom-left',
        effect: 'genie'
      });
      if (callback) {
        callback();
      }
      return;
    }
    var notEmpty = false;
    dataObjs.forEach(object => {
      object.bndBoxes = object.rects.map(rect => {
        let xmin = rect.left / this._canvas.cropMode.canvasWidth;
        let ymin = rect.top / this._canvas.cropMode.canvasHeight;
        let xmax = (rect.left + rect.width) / this._canvas.cropMode.canvasWidth;
        let ymax = (rect.top + rect.height) / this._canvas.cropMode.canvasHeight;
        if (!(Utils.isNumber(xmin) &&
              Utils.isNumber(xmax) &&
              Utils.isNumber(ymin) &&
              Utils.isNumber(ymax))) {
          Alert.error(`非法数据！${JSON.stringify(rect)}`, {
            position: 'bottom-left',
            effect: 'genie'
          });
          console.log(rect);
          throw new Error('invalid rect');
        }
        if (xmin < 0)
          xmin = 0;
        if (ymin < 0)
          ymin = 0;
        if (xmax > 1)
          xmax = 1;
        if (ymax > 1)
          ymax = 1;
        notEmpty = true;
        let box = {
          xmin: xmin,
          ymin: ymin,
          xmax: xmax,
          ymax: ymax,
          attrs: rect.attrs,
          stroke: rect._stroke
        };
        if (rect.upstream) {
          box.uid = rect.uid;
        }
        return box;
      });
    });
    var postData;
    var imgParams = this._canvas.cropMode.imageParams;
    var angle = 0;
    if (imgParams && imgParams.angle) {
      angle = imgParams.angle;
    }
    postData = {
      unitid: this._data.unitid,
      url: this._data.url,
      imgid: this._data.imgid,
      operation: 'alter',
      objs: dataObjs.map(object => {
        return {
          name: object.name,
          uid: object.uid,
          geometry: object.geometry,
          bndBoxes: object.bndBoxes
        };
      }).filter(object => {
        return object.bndBoxes.length;
      })
    };
    if (angle)
      postData.angle = angle;
    /* if (notEmpty) {
     *   postData = {
     *     unitid: this._data.unitid,
     *     url: this._data.url,
     *     imgid: this._data.imgid,
     *     operation: 'alter',
     *     objs: this._data.objs.map(object => {
     *       return {
     *         name: object.name,
     *         uid: object.uid,
     *         geometry: object.geometry,
     *         bndBoxes: object.bndBoxes
     *       };
     *     }).filter(object => {
     *       return object.bndBoxes.length;
     *     })
     *   };
     * } else {
     *   postData = {
     *     unitid: this._data.unitid,
     *     url: this._data.url,
     *     imgid: this._data.imgid,
     *     operation: 'skip',
     *   };
     * }*/
    // Some required attributes are not provided.
    postJSONPromise(this.URLWithQuery, postData).then(data => {
      this.hasChanged = false;
      if (callback)
        callback(data);
    });
  }
  triggleCommand(cmd, silent) {
    this._config.triggleCommand(cmd, silent);
  }
  triggleZoomMode(turnOff) {
    if (!turnOff) {
      this._zoomMode = true;
      this._config.triggleZoomMode();
    } else {
      this._zoomMode = false;
      this._config.triggleZoomMode(turnOff);
    }
  }
  setFooterOffset(offset) {
    this.setState({footerOffset: offset});
  }
  componentDidMount() {
    // Components binding.
    this._pagination = this.refs.pagination;
    this._canvas = this.refs.canvas;
    this._attrsBox = this.refs.attrsBox;
    this._config = this.refs.config;

    this._canvas.cropMode.stroke = this._config.getColor();
    this._config.triggleCommand();
    this._updateCurrentSlice();
  }
  render() {
    let maxHeightSpan = this.props.maxHeightSpan || 600;
    let footerStyle = {
      top: `${maxHeightSpan}px`
    };
    if (this.state.footerOffset)
      footerStyle.top = `${this.state.footerOffset}px`;
    let leftStyle = {
      left: `${maxHeightSpan == 600 ? 75 : 65}%`
    };
    return (
      <div id="image-tagger">
        <PictureEdit
          ref="canvas"
          maxHeightSpan={ this.props.maxHeightSpan }
          onCropSelected={ this._handleCropSelected.bind(this) }
          rectZoom={ this.props.rectZoom }
          triggleCommand={ this.triggleCommand.bind(this) }
          triggleZoomMode={ this.triggleZoomMode.bind(this) }
          setFooterOffset={ this.setFooterOffset.bind(this) }
          tools={ this.props.tools }
          onPageInc={
            (step) => {
              this._pagination.incKey(step);
            }
                    }
          style={{ left: this.state.canvasLeft + 'px' }}
        />
        <div className="image-tagger-side-div" style={ leftStyle }>
          <ControlPannel
            mode={ this.props.tools }
            onCommand={ this._handleCommand.bind(this) }
            ref="config"
          />
          <AttrsBox
            ref="attrsBox"
            onAttrChange={ this._handleAttrChange.bind(this) }
            onSelectedObjectChange={ this._handleSelectedObjectChange.bind(this) }
          />
          <p className="image-tagger-reference-info">{ this.state.description }
            { this.state.referURL ?
              <a target="_blank" href={ this.state.referURL }> 图片源地址</a> :
              ''
            }
          </p>
        </div>
        <div className="image-tagger-footer" style={ footerStyle }>
          <Pagination
            onChange={ this._handlePageChange.bind(this) }
            beforeChange={ this._handleBeforePageChange.bind(this) }
            onIndicatorClicked={ this._handlePageIndicatorClicked.bind(this) }
            onSubmit={ this._handleSubmit.bind(this) }
            onSkip={ this._handleSkip.bind(this) }
            ref="pagination"
          />
        </div>
        <div id="image-tagger-modal"></div>
        <Alert stack={ {limit: 3} } />
      </div>
    )
  }
}

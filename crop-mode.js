import { fabric } from 'fabric';
import * as Utils from './utils.js';

var CropRect = fabric.util.createClass(fabric.Rect, {
  type: 'CropRect',
  initialize: function (options) {
    var defaultOptions = {
      originX: 'left',
      originY: 'top',
      transparentCorners: false,
      lockRotation: true,
      hasRotatingPoint: false,
      hasBorders: false,
      borderColor: '#505050',
      cornerColor: '#505050',
      fill: 'transparent',
      stroke: 'black',
      strokeWidth: 2,
      selection: false,
      fontSize: 14,
      name: 'Crop REct',
      zoomLevel: 1,
      cornerSize: 6
    };
    options = Utils.extendDict(options, defaultOptions);
    this.objectId = options.objectId; // Public objectId
    this.upstream = options.upstream;

    this.uid = options.uid;
    this.fontSize = options.fontSize;
    this.boxIndex = options.index;
    this._stroke = options.stroke;
    this._strokeWidth = options.strokeWidth;
    this._zoomLevel = options.zoomLevel;
    this.name = options.name || '';
    if (options.editable === false)
      this._editable = false;
    else
      this._editable = true;
    this.description = options.description || '';
    this.callSuper('initialize', options);
    var t = options.hasTransient;
    this._taggerIsTransient = typeof t === 'boolean' ? t : true;
    this.set('objectType', options.objectType || '');
    this.on('deselected', this._handleDeselected.bind(this));
    this.on('selected', this._handleSelected.bind(this));
  },
  lockUp: function () {
    this.lockMovementX = true;
    this.lockMovementY = true;
    this.lockScalingFlip = true;
    this.lockScalingX = true;
    this.lockScalingY = true;
    this.lockSkewingX = true;
    this.lockSkewingY = true;
    this.lockUniScaling = true;
    this.selectable = false;
    this.hoverCursor = 'default';
  },
  unlock: function () {
    this.selectable = true;
    this.hoverCursor = 'pointer';
    if (!this._editable) {
      console.log('ImageTaggerWarning: crop not editable.');
      return;
    }
    this.lockMovementX = false;
    this.lockMovementY = false;
    this.lockScalingFlip = false;
    this.lockScalingX = false;
    this.lockScalingY = false;
    this.lockSkewingX = false;
    this.lockSkewingY = false;
    this.lockUniScaling = false;
  },
  acomplish: function () {
    this.fill = 'transparent';
    this._taggerIsTransient = false;
    this.setCoords();
  },
  _render: function (ctx) {
    if (this._zoomLevel) {
      this.strokeWidth = this._strokeWidth / this._zoomLevel;
    }
    this._ctx = ctx;
    this.callSuper('_render', ctx);
    if (this.scaleX !== 1) {
      this.width = this.width * this.scaleX;
      this.scaleX = 1;
    }
    if (this.scaleY !== 1) {
      this.height = this.height * this.scaleY;
      this.scaleY = 1;
    }

    // Set original scale
    var flipX = this.flipX ? -1 : 1;
    var flipY = this.flipY ? -1 : 1;
    var scaleX = flipX / this.scaleX;
    var scaleY = flipY / this.scaleY;
    ctx.scale(scaleX, scaleY);
    if (this._taggerIsTransient)
      this._renderGrid(ctx);
    this._renderText(ctx);
    ctx.scale(1 / scaleX, 1 / scaleY);
  },
  _renderText: function (ctx) {
    ctx.save();
    ctx.translate(-this.getWidth() / 2, -this.getHeight() / 2);
    ctx.font = `${this.fontSize || 14}px sans-serif`;
    ctx.scale(1 / this._zoomLevel, 1 / this._zoomLevel);
    ctx.fillStyle = this.stroke;
    var text = this.name + ' ' + this.description;
    var x = 4, y = 16;
    ctx.fillText(text, x, y);
    ctx.restore();
  },
  _renderGrid: function (ctx) {
    var drawLine = function (x, y, tx, ty) {
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(tx, ty);
      ctx.stroke();
    }
    var width = this.getWidth();
    var height = this.getHeight();
    ctx.save();
    var lineWidth = 2;
    if (this._zoomLevel)
      lineWidth = lineWidth / this._zoomLevel;
    ctx.lineWidth = lineWidth;
    ctx.strokeStyle = 'rgba(255, 255, 255, 0.4)';
    ctx.translate(-this.getWidth() / 2, -this.getHeight() / 2);
    drawLine(width / 3, 0, width / 3, height);
    drawLine(width * 2 / 3, 0, width * 2 / 3, height);
    drawLine(0, height / 3, width, height / 3);
    drawLine(0, 2 * height / 3, width, 2 * height / 3);
    ctx.restore();
  },
  setZoom: function (ratio) {
    this._zoomLevel = ratio;
  },
  setStroke: function (stroke) {
    this._stroke = stroke;
    this.stroke = stroke;
  },
  _handleSelected: function (options) {
    if (this._taggerIsTransient) {
      return;
    }
  },
  _handleDeselected: function (options) {
    this.stroke = this._stroke;
    this._taggerIsTransient = false;
    this.set({
      fill: 'transparent'
    });
  }
});

var imageURLStore = null;
//********************************************************************
// CropMode
// The CropMode base class handles all basic status of crop mode
// instances. But it's more of an abstract class. Any implement should
// handle the rects construction by implement `_preTagMode` method and
// `_afterTagMode`.
//********************************************************************
class CropMode {
  constructor(canvas, options) {
    if (this.constructor === CropMode) {
      var msg = 'Abstract class "CropMode" ' +
                'cannot be instantiated directly.';
      throw new TypeError(msg);
    }
    if (!(canvas instanceof fabric.Canvas)) {
      var msg = 'CropMode constructor must take a Canvas instance.';
      throw new TypeError(msg);
    }
    this._canvas = canvas;
    this._containerElement = this._canvas.getElement().parentNode;
    this._imageElement = document.getElementById(
      'image-tagger-original-image'
    );
    if (!this._imageElement) {
      this._imageElement = document.createElement('img');
      this._imageElement.style.visibility = 'hidden';
      this._imageElement.style.position = 'fixed';
      this._imageElement.style.top = 0;
      this._imageElement.style.left = 0;
      this._imageElement.id = 'image-tagger-original-image';
      this._containerElement.appendChild(this._imageElement);
    }
    this._imageObject = new fabric.Image();
    this._stroke = options && options.stroke || 'black';
    this._strokeWidth = options && options.strokeWidth || 2;
    this._workMode = null;
    this._indirectCrops = [];
    this._name = options && options.name || null;
    this._description = options && options.description || null;
    this._objectId = options && options.objectId || null;
    this._maxHeightSpan = options && options.maxHeightSpan || 600;

    this._handleCropSelected = null;

    // Canvas initialize.
    this._canvas.selection = false;
    this._canvas.selectionLineWidth = 0;
    
    this._handleContainerContextMenu =
      this._handleContainerContextMenu.bind(this);
    this._canvas.getElement().parentNode.addEventListener(
      'contextmenu',
      this._handleContainerContextMenu
    );

    this._zoomModeMouseDown = this._zoomModeMouseDown.bind(this);
    this._zoomModeMouseMove = this._zoomModeMouseMove.bind(this);
    this._zoomModeMouseUp = this._zoomModeMouseUp.bind(this);
  }
  // Set name for current selected crops.
  set name(name) {
    if (typeof name !== 'string') {
      console.error('ImageTaggerError: crop name should be string.');
      throw new Error('Bad Argument!');
    }
    this._name = name;
    this.getActiveCrops().forEach(crop => {
      crop.name = name;
    });
  }
  get name() {
    return this._name;
  }
  // Set object for current selected crops.
  set objectId(objectId) {
    this._objectId = objectId;
    this.getActiveCrops().forEach(crop => {
      crop.objectId = objectId;
    });
  }
  set attrs(attrs) {
    let objId = null;
    this.getActiveCrops().forEach(crop => {
      if (!objId)
        objId = crop.objectId;
      else if (objId != crop.objectId)
        throw new Error('存在不一致的分类');
    });
    this.getActiveCrops().forEach(crop => {
      crop.attrs = JSON.parse(JSON.stringify(attrs));
    });
  }
  // Set description for current selected crops.
  set description(description) {
    if (typeof description !== 'string') {
      console.error('ImageTaggerError: crop description should be string.');
      throw new Error('Bad Argument!');
    }
    this._description = description;
    this.getActiveCrops().forEach(crop => {
      crop.description = description;
    });
  }
  get stroke() {
    return this._stroke;
  }
  set stroke(stroke) {
    if (typeof stroke !== 'string') {
      console.error('ImageTaggerError: CropMode.stroke should be string.');
      throw new Error('Bad Argument!');
    }
    this._stroke = stroke;
    this.getActiveCrops().forEach(crop => {
      crop.setStroke(stroke);
    });
  }
  get strokeWidth() {
    return this._strokeWidth;
  }
  set strokeWidth(strokeWidth) {
    if (typeof strokeWidth !== 'number') {
      console.error('ImageTaggerError: CropMode.strokeWidth should be number.');
      throw new Error('Bad Argument!');
    }
    this._strokeWidth = strokeWidth;
  }
  set onCropSelected(callback) {
    this._handleCropSelected = function (options) {
      let crop = options.target;
      crop.stroke = Utils.invertRGB(
        Utils.rgbColor(crop._stroke)
      );
      var boxIndex = options.target.boxIndex;
      if (typeof boxIndex !== 'number') return;
      callback(this._crops[boxIndex]);
      this.refresh();
    }.bind(this);
    this._canvas.on('object:selected', this._handleCropSelected);
  }
  clearCanvas() {
    this._canvas.clear();
    this._crops = [];
  }
  getRects() {
    return this._validCrops.map(crop => {
      var box = {
        objectId: crop.objectId,
        stroke: crop._stroke,
        strokeWidth: crop._strokeWidth,
        name: crop.name,
        width: crop.width / this._canvas.getWidth(),
        height: crop.height / this._canvas.getHeight(),
        left: crop.left / this._canvas.getWidth(),
        top: crop.top / this._canvas.getHeight(),
        attrs: crop.attrs
      };
      if (crop.uid) {
        box.uid = crop.uid;
      }
      return box;
    });
  }
  getCrops() {
    return this._validCrops;
  }
  // Add single crop or multiple crops as an array into canvas.
  // Derived class should always use this method instead of add crop
  // directly, because this method allocates unque id for crop.
  addCrops(rects, optionalFields) {
    optionalFields = optionalFields || [];
    var index = null;
    if (!(rects instanceof Array)) {
      var rect = rects;
      rect.hasTransient = false;
      index = this._addCrop(rect);
      optionalFields.forEach(field => {
        this._crops[index][field] = rect[field];
      });
      this._crops[index].acomplish();
      this._crops[index].lockUp();
    } else {
      rects.forEach(rect => {
        rect.hasTransient = false;
        var i = this._addCrop(rect);
        optionalFields.forEach(field => {
          this._crops[i][field] = rect[field];
        });
        this._crops[i].acomplish();
        this._crops[i].lockUp();
        if (!index) index = i;
      });
    }
    if (this._workMode === 'select-mode') {
      this.selectMode();
    }
    return index;
  }
  clearOthers() {
    this._canvas.discardActiveGroup();
    this._canvas.getObjects('SelectPoint').forEach(point => {
      this._canvas.remove(point);
    });
    if (this._clickCount)
      this._clickCount = 0;
    this.refresh();
  }
  // Remove crop by uid.
  removeCrop(uid) {
    this._crops[uid].remove();
    this._crops[uid] = null;
  }
  // Remove selected crops.
  removeCrops() {
    this.getActiveCrops().forEach(crop => {
      this._crops[crop.boxIndex] = null;
      crop.remove();
    });
    this._canvas.discardActiveGroup();
  }
  refresh() {
    this._canvas.renderAll();
  }
  // Enter tag mode.
  // NOTE: You should refresh to enable the changes.
  tagMode() {
    if (this._workMode === 'tag-mode') return;
    this._deactivateAll();
    this._canvas.selectionLineWidth = 0;
    this._canvas.selection = false;
    this._validCrops.forEach(crop => {
      crop.lockUp();
    });
    this._preTagMode();
    this._workMode = 'tag-mode';
  }
  // This method should be called whenever a CropMode
  // instance is abandened.
  cleanup() {
    this._orphanCare();
    if (this._afterTagMode)
      this._afterTagMode();
    this._validCrops.forEach(crop => {
      crop.remove();
    });
    this._canvas.getElement().parentNode.removeEventListener(
      'contextmenu',
      this._handleContainerContextMenu
    );
    if (this._handleCropSelected)
      this._canvas.off('object:selected', this._handleCropSelected);
  }
  getActiveCrops() {
    var activeObj = this._canvas.getActiveObject();
    var activeGrp = this._canvas.getActiveGroup();
    if (activeGrp)
      return activeGrp.getObjects();
    if (activeObj)
      return [ activeObj ];
    return [];
  }
  setActiveCrop(index) {
    let crop = this._crops[index];
    while (!crop && index < this._crops.length * 2) {
      crop = this._crops[(++index) % this._crops.length];
    }
    if (crop)
      this._canvas.setActiveObject(crop);
  }
  setActiveCrops(indexList) {
    if (indexList.length === 1) {
      this.setActiveCrop(indexList);
      return;
    }
    this._canvas.discardActiveGroup();
    var crops = indexList.map(index => {
      return this._crops[index];
    });
    var group = new fabric.Group(crops);
    this._canvas.setActiveGroup(group.setCoords());
  }
  get naturalWidth() {
    return this._naturalWidth;
  }
  get naturalHeight() {
    return this._naturalHeight;
  }
  get canvasWidth() {
    return this._canvas.getWidth();
  }
  get canvasHeight() {
    return this._canvas.getHeight();
  }
  _handleContainerContextMenu(event) {
    if (this._workMode != 'tag-mode' && this._workMode != 'select-mode')
      return;
    event.preventDefault();
    var point = new fabric.Point(event.layerX, event.layerY);
    var crop;
    var validCrops = this._validCrops;
    for (var i in validCrops) {
      if (validCrops[i].containsPoint(point)) {
        if (!crop) {
          crop = validCrops[i];
          continue;
        }
        if (validCrops[i].getWidth() < crop.getWidth() &&
            validCrops[i].getHeight() < crop.getHeight())
          crop = validCrops[i];
        break;
      }
    }
    if (crop) {
      this._deactivateAll();
      this.setActiveCrop(crop.boxIndex);
    } else {
      this._deactivateAll();
    }
  }
  // Update background image.
  // NOTE: You should refresh to enable the changes.
  updateImage(imageURL, params, callback) {
    this.imageParams = params;
    let angle = params.angle;
    if (!angle) {
      angle = 0;
    } else {
      angle = parseInt(angle / 90) * 90;
    }
    this.imageParams.angle = angle;
    if (!/https?:\/\//.test(imageURL)) {
      throw new Error('Bad image URL.');
    }
    this._imageElement.src = imageURL;
    imageURLStore = imageURL;
    this.imageURL = imageURL;
    var haveExecuted = false;
    this._imageElement.addEventListener('load', () => {
      if (haveExecuted) return;
      haveExecuted = true;
      this._naturalWidth = this._imageElement.naturalWidth;
      this._naturalHeight = this._imageElement.naturalHeight;
      let naturalWidth = this._naturalWidth;
      let naturalHeight = this._naturalHeight;
      if (angle % 180) {
        naturalWidth = this._naturalHeight;
        naturalHeight = this._naturalWidth;
      }
      var height, width;
      if (!params.originalSize) {
        var ratio = naturalWidth / naturalHeight;

        if (ratio < 16 / 9) {
          height = this._maxHeightSpan || 600;
          width = height * ratio;
        } else {
          width = (this._maxHeightSpan || 600) * 16 / 9;
          height = width / ratio;
        }
      } else {
        width = naturalWidth;
        height = naturalHeight;
        this.setFooterOffset(height);
      }
      this._imageElement.style.display = 'none';
      this._canvas.setHeight(height);
      this._canvas.setWidth(width);
      let backWidth = width;
      let backHeight = height;
      if (angle % 180) {
        backWidth = height;
        backHeight = width;
      }
      this._imageObject.setElement(this._imageElement, null, {
        height: backHeight,
        width: backWidth,
        left: width / 2,
        top: height / 2,
        angle: angle,
        selectable: false
      });
      this._canvas.setBackgroundImage(this._imageObject, callback, {
        originX: 'center',
        originY: 'center'
      });
    });
    this._imageElement.addEventListener('error', (event) => {
      event.target.src = imageURL.replace(
        'img.adkalava.com', 'pic.adkalava.com'
      );
    });
  }
  setZoomBackground(zoomRect) {
    var canvasWidth = this._canvas.getWidth(),
        canvasHeight = this._canvas.getHeight();
    var query = {
      x: zoomRect.left,
      w: zoomRect.width,
      y: zoomRect.top,
      h: zoomRect.height,
      url: this.imageURL
    };
    let reverse = false;
    if (this.imageParams.angle) {
      let angle = 360 - this.imageParams.angle;
      switch (angle) {
        case 90:
          var x = canvasHeight - query.h - query.y;
          var y = query.x;
          break;
        case 180:
          var x = canvasWidth - query.x - query.w;
          var y = canvasHeight - query.y - query.h;
          break;
        case 270:
          var x = query.y;
          var y = canvasWidth - query.x - query.w;
          break;
      }
      var w = query.w;
      var h = query.h;
      if (angle % 180) {
        w = query.h;
        h = query.w;
        reverse = true;
      }
      query.x = x;
      query.w = w;
      query.y = y;
      query.h = h;
    }
    let backOptions = {
      originX: 'center',
      originY: 'center',
      angle: this.imageParams.angle || 0,
      width: zoomRect.width,
      height: zoomRect.height,
      left: zoomRect.left + zoomRect.width / 2,
      top: zoomRect.top + zoomRect.height / 2
    };
    if (reverse) {
      backOptions.height = zoomRect.width;
      backOptions.width = zoomRect.height;
      query.x /= canvasHeight;
      query.w /= canvasHeight;
      query.y /= canvasWidth;
      query.h /= canvasWidth;
    } else {
      query.x /= canvasWidth;
      query.w /= canvasWidth;
      query.y /= canvasHeight;
      query.h /= canvasHeight;
    }
    var url = Utils.cropImage(query);
    /* fabric.Image.fromURL(url, imgObject => {
     *   imgObject.set(backOptions);
     *   this._canvas.add(imgObject);
     * });*/
    this._canvas.setBackgroundImage(
      url, this._canvas.renderAll.bind(this._canvas), backOptions
    );
    window.canvas = this._canvas;
  }
  resetBackground() {
    this.updateImage(this._imageElement.src, this.imageParams, () => {
      this.refresh();
    });
    /* this._canvas.setBackgroundImage(
     *   this.imageURL || imageURLStore,
     *   this._canvas.renderAll.bind(this._canvas),
     *   {
     *     originX: 'left',
     *     originY: 'top',
     *     selectable: false,
     *     width: this._canvas.getWidth(),
     *     height: this._canvas.getHeight(),
     *   }
     * );*/
  }
  rotate() {
    if (this._zoomLevel !== undefined && this._zoomLevel !== 1) {
      this.exitZoom();
    }
    if (!this.imageParams) {
      return;
    }
    this.imageParams.angle = ((this.imageParams.angle || 0) + 90) % 360;
    this.getCrops().forEach(crop => {
      if (!crop.origWidth && this.imageParams.angle == 90) {
        ['left', 'top', 'width', 'height'].forEach(k => {
          crop[`orig${k.charAt(0).toUpperCase() +  k.slice(1)}`] = crop[k];
        });
      }
      let canvasWidth = this._canvas.getWidth(),
          canvasHeight = this._canvas.getHeight();
      let x = (canvasHeight - crop.height - crop.top) / canvasHeight;
      let y = crop.left / canvasWidth;
      let w = crop.height / canvasHeight;
      let h = crop.width / canvasWidth;
      crop._x = x;
      crop._y = y;
      crop._w = w;
      crop._h = h;
    });
    this.updateImage(this._imageElement.src, this.imageParams, () => {
      let canvasWidth = this._canvas.getWidth(),
          canvasHeight = this._canvas.getHeight();
      this.getCrops().forEach(crop => {
        crop.left = crop._x * canvasWidth;
        crop.top = crop._y * canvasHeight;
        crop.width = crop._w * canvasWidth;
        crop.height = crop._h * canvasHeight;
        crop.setCoords();
      });
      this.refresh();
    });
  }
  zoomMode() {
    this._orphanCare();
    if (this._afterTagMode)
      this._afterTagMode();
    if (this._workMode !== 'zoom-mode' && this._preZoomMode)
      this._preZoomMode();
    this._workMode = 'zoom-mode';
  }
  _preZoomMode() {
    this._validCrops.forEach(crop => {
      crop.lockUp();
    });

    this._canvas.selection = false;
    this._canvas.on('mouse:down', this._zoomModeMouseDown);
    this._canvas.on('mouse:move', this._zoomModeMouseMove);
    this._canvas.on('mouse:up', this._zoomModeMouseUp);
  }
  _afterZoomMode() {
    this._canvas.off('mouse:down', this._zoomModeMouseDown);
    this._canvas.off('mouse:move', this._zoomModeMouseMove);
    this._canvas.off('mouse:up', this._zoomModeMouseUp);
  }
  _zoomModeMouseDown(options) {
    if (this._zoomRect || this._workMode !== 'zoom-mode')
      return;
    this._isMouseDown = true;
    var pointer = this._canvas.getPointer(options.e);
    this._origX = pointer.x < 0 ? 0 : pointer.x;
    this._origY = pointer.y < 0 ? 0 : pointer.y;
    var pointer = this._canvas.getPointer(options.e);

    var square = new fabric.Rect({
      width: pointer.x - this._origX,
      height: pointer.y - this._origY,
      left: this._origX,
      top: this._origY,
      transparentCorners: false,
      lockRotation: true,
      hasRotatingPoint: false,
      hasBorders: false,
      borderColor: '#505050',
      cornerColor: '#505050',
      selection: false,
      hoverCursor: 'default',
      fill: 'rgba(1, 107, 220, 0.4)'
    });

    this._zoomRect = square;
    this._canvas.add(square);
    this.refresh();
  }
  _zoomModeMouseMove(options) {
    if (!this._isMouseDown || this._workMode !== 'zoom-mode')
      return;
    var pointer = this._canvas.getPointer(options.e);
    if (pointer.x < 1) pointer.x = 1;
    if (pointer.y < 1) pointer.y = 1;
    if (pointer.x >= this._canvas.getWidth())
      pointer.x = this._canvas.getWidth() - 1;
    if (pointer.y >= this._canvas.getHeight())
      pointer.y = this._canvas.getHeight() - 1;
    var left = this._origX, top = this._origY;
    if (pointer.x < this._origX) {
      left = pointer.x;
    }
    if (pointer.y < this._origY) {
      top = pointer.y;
    }
    var width = Math.abs(pointer.x - this._origX);
    var height = Math.abs(pointer.y - this._origY);

    this._zoomRect.set({
      left: left,
      top: top,
      width: width,
      height: height
    });
    this._zoomRect.setCoords();
    this.refresh();
  }
  _zoomModeMouseUp(options) {
    if (!this._zoomModeMouseDown || this._workMode !== 'zoom-mode')
      return;
    this._canvas.setActiveObject(this._zoomRect);
    this._isMouseDown = false;

    this.rectZoom(this._zoomRect);

    this._zoomRect.remove();
    this._zoomRect = null;
    this.refresh();
  }
  rectZoom(zoomRect) {
    var zoomWidth = zoomRect.width;
    var zoomHeight = zoomRect.height;
    var canvasWidth = this._canvas.getWidth();
    var canvasHeight = this._canvas.getHeight();

    var ratio = Math.min(canvasWidth / zoomWidth, canvasHeight / zoomHeight);
    this.getCrops().forEach(crop => {
      crop.setZoom(ratio);
    });
    this._zoomLevel = ratio;
    var panX = zoomRect.left;
    var panY = zoomRect.top;
    if (canvasWidth / zoomWidth > ratio) {
      panX -= (canvasWidth / ratio - zoomWidth) / 2;
      if (panX < 0)
        panX = 0;
    } else if (canvasHeight / zoomHeight > ratio) {
      panY -= (canvasHeight / ratio - zoomHeight) / 2;
      if (panY < 0)
        panY = 0;
    }

    zoomRect.left = panX;
    zoomRect.top = panY;
    zoomRect.width = canvasWidth / ratio;
    zoomRect.height = canvasHeight / ratio;
    this.setZoomBackground(zoomRect);

    this._canvas.absolutePan({x: panX, y: panY});
    this._canvas.setZoom(ratio);
    this._afterZoomMode();
    if (this._workMode !== 'tag-mode' && this._preTagMode)
      this._preTagMode();
    if (this.afterZoomMode)
      this.afterZoomMode();
    this.controlPannelZoomMode();
  }
  exitZoom() {
    this._canvas.absolutePan({x: 0, y: 0});
    this._canvas.setZoom(1);
    this.resetBackground();
    this._zoomLevel = 1;
    this.getCrops().forEach(crop => {
      crop.setZoom(1);
    });

    this._canvas.renderAll();
  }
  // Enter select mode.
  // NOTE: You should refresh to enable the changes.
  selectMode() {
    this._orphanCare();
    this._validCrops.forEach(crop => {
      crop.unlock();
    });
    if (this._afterTagMode)
      this._afterTagMode();
    this._workMode = 'select-mode';
  }
  _orphanCare() {
    var crops = this.getActiveCrops();
    crops.forEach(crop => {
      // Use deselected event to trigger acomplish.
      // Not perfect but got to.
      crop.trigger('deselected');
    });
  }
  // Add crop into canvas.
  // Rect format:
  // width, height, left, right, stroke (optional),
  // strokeWidth (optional), name (optional).
  // NOTE: You should refresh to enable the changes.
  _addCrop(rect) {
    if (!rect.strokeWidth) rect.strokeWidth = this._strokeWidth;
    if (!rect.stroke) rect.stroke = this._stroke;
    if (!rect.zoomLevel) rect.zoomLevel = this._zoomLevel;
    rect = this._normalize(rect);
    var index = this._crops.length;
    rect.index = index;
    if (!rect.name)
      rect.name = this._name || '';
    if (typeof rect.objectId !== 'number' || Number.isNaN(rect.objectId))
      rect.objectId = this._objectId;
    var crop = new CropRect(rect);
    this._crops[index] = crop;
    this._canvas.add(crop);
    return index;
  }
  // TODO deactivateAllWithDispatch
  _deactivateAll() {
    this._canvas.discardActiveGroup();
    this._orphanCare();
    var tempRect = new fabric.Rect();
    this._canvas.add(tempRect);
    this._canvas.setActiveObject(tempRect);
    tempRect.remove();

  }
  get _crops() {
    return this._indirectCrops;
  }
  set _crops(crops) {
    this._indirectCrops = crops;
  }
  get _validCrops() {
    var crops = this._indirectCrops.filter(crop => {
      return crop;
    });
    return crops;
  }
  // Convert scaled width, height, left, top into pixels.
  _normalize(options) {
    if (Math.abs(options.width) > 1) return options;
    options.width *= this._canvas.getWidth();
    options.left *= this._canvas.getWidth();
    options.height *= this._canvas.getHeight();
    options.top *= this._canvas.getHeight();
    return options;
  }
}

export { CropMode, CropRect };

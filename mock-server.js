"use strict";

var port = normalizePort(process.env.PORT || '7334');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

app.use(bodyParser.json());
app.use(function (req, res, next) {
  console.log("Request to path " + req.path + "\n");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Max-Age", 86400);
  next();
});

var image = {
  "errcode": 0,
  "unitid": 34798,
  "imgid": "10393551",
  "maxseqid": 34,
  "url": "http:\/\/img.adkalava.com\/img006\/343\/40a5c1336fdcd0bf_300x300.jpg",
  "optype":"tag",
  "objs":[
    {
      "name":"\u6c7d\u8f66",
      "uid":3654,
      "bndBoxes":[
        {
          "xmin":0.7607,
          "ymin":0.2934,
          "xmax":0.8154,
          "ymax":0.3663,
          "uid":53748734
        }
      ],
      "attrs":[]
    },{
      "name":"\u81ea\u884c\u8f66",
      "type":1,
      "uid":3655,
      "attrs":[]
    },
    {
      "name":"\u516c\u4ea4\u8f66",
      "type":1,
      "uid":3656,
      "attrs":[]
    },{
      "name":"\u5361\u8f66",
      "type":1,
      "uid":3657,
      "attrs":[]
    }]
};
var urls = [
  'http://pic.adkalava.com/img002/51/b5b89bb7932845e3.jpg',
  'http://pic.adkalava.com/img002/42/a3af7f20c23b58c2.jpg',
  'http://pic.adkalava.com/img002/31/25d1fd702a38fc0f.jpg',
  'http://pic.adkalava.com/img002/79/6af77024ab35b35f.jpg',
  'http://pic.adkalava.com/img002/56/e53033b30d53b138.jpg',
  'http://pic.adkalava.com/img002/94/6eab3b4cd0f26746.jpg',
  'http://pic.adkalava.com/img002/63/4ae6dbb08ed82b17.jpg'
];

var count = 0;

app.get("/image", function (request, response) {
  console.log("images GET request " + JSON.stringify(request.body, undefined, 4) + "\n");
  count = (count + 1) % urls.length;
  image.url = urls[count];
  response.json(image);
});

app.post("/image", function (request, response) {
  console.log("images POST request " + JSON.stringify(request.body, undefined, 4) + "\n");
  count = (count + 1) % urls.length;
  image.url = urls[count];
  response.json(image);
});


app.listen(port, function () {
  console.log("Images mock server @" + port);
});
